class Array
    def subtract_once(values)
    counts = values.inject(Hash.new(0)) {|h,v| h[v]+=1; h}
    reject {|e| counts[e] -= 1 unless counts[e].zero?}
    end
end

class MasterMind
    
    attr_accessor :player_choice, :computer_choice, :pass_to_white_master_mind, :pass_to_white_decoder
    COLOR_ARR= ["green","red","yellow", "blue","black","white"]

    def initialize(playername)
        @playername=playername
        @player_choice=[]
        @computer_choice=[]
        @black_pins=[]
        @white_pins=[]
    end

    def input_from_player()
        4.times do
            puts "plz write your color choices"
            @player_choice<< gets.chomp
        end
    end

    def sample_picker()
        ready_to_flatt = COLOR_ARR.sample(4)
        @computer_choice = ready_to_flatt.flatten
    end

    def final_check(master_mind, decoder)
        boolean_arr = master_mind.zip(decoder).map {|x,y| x==y}
        if boolean_arr.include? (false)
            return false
        end
            return true
    end

    def give_black_pin(master_mind,decoder)
        boolean_arr = master_mind.zip(decoder).map {|x,y| x==y}
        @black_pins<< boolean_arr.count(true)

        reject =[]
        boolean_arr.zip(master_mind).map {|a,y| reject<<y if a==true }
    
        @pass_to_white_master_mind = master_mind.subtract_once(reject)
        @pass_to_white_decoder = decoder.subtract_once(reject)
    
    end


    def give_white_pin(master_mind,decoder)

        difference_arr = master_mind.subtract_once(decoder)
        ready_white_pin = master_mind.subtract_once(difference_arr)
        @white_pins << ready_white_pin.length

    end

    def master_mind_help()
        puts " you have #{@black_pins.last} black pins(right color, right place)"
        puts " you have #{@white_pins.last} white pins(right color but wrong place)"
    end

    def cheat_code()
        puts "psss here is the cheat code #{@computer_choice}"
    end

    def round()
        self.input_from_player()
        self.give_black_pin(@computer_choice,@player_choice)
        self.give_white_pin(@pass_to_white_master_mind,@pass_to_white_decoder)
        self.master_mind_help()
        self.cheat_code()
        @player_choice=[]
    end

    def start_game()
        puts "the game is ON"
        self.sample_picker()

        12.times do
            if  @black_pins.last==4
                puts "you won the game"
                return
            end
            self.round()
        end
        
    end
end

shin = MasterMind.new("shahab")
shin.start_game()


